﻿using RPGCharacters.Models.Characters;
using System;
using System.Text;

namespace RPGCharacters.Util
{
    public class CharacterWriter
    {
        public void PrintCharacterStatsToConsole(CharacterBase character)
        {
            StringBuilder stringBuilder = new StringBuilder();

            stringBuilder.AppendLine("------- Character stats -------");
            stringBuilder.AppendLine($"Name: {character.Name}");
            stringBuilder.AppendLine($"Level: {character.Level}");
            stringBuilder.AppendLine("Primary stats");
            stringBuilder.AppendLine($"Strength: {character.TotalPrimaryAttributes.Strength}");
            stringBuilder.AppendLine($"Dexterity: {character.TotalPrimaryAttributes.Dexterity}");
            stringBuilder.AppendLine($"Intelligence: {character.TotalPrimaryAttributes.Intelligence}");
            stringBuilder.AppendLine("Secondary stats");
            stringBuilder.AppendLine($"Health: {character.SecondaryAttributes.Health}");
            stringBuilder.AppendLine($"Armor Rating: {character.SecondaryAttributes.ArmorRating}");
            stringBuilder.AppendLine($"Elemental Resistance: {character.SecondaryAttributes.ElementalResistance}");

            Console.WriteLine(stringBuilder.ToString());
        }
    }
}
