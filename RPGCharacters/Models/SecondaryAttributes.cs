﻿using System;

namespace RPGCharacters.Models
{
    public class SecondaryAttributes
    {
        public double Health { get; set; }
        public double ArmorRating { get; set; }
        public double ElementalResistance { get; set; }

        public override bool Equals(object obj)
        {
            return obj is SecondaryAttributes attributes &&
                   Health == attributes.Health &&
                   ArmorRating == attributes.ArmorRating &&
                   ElementalResistance == attributes.ElementalResistance;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Health, ArmorRating, ElementalResistance);
        }
    }
}
