﻿namespace RPGCharacters.Models.Items
{
    public class Weapon : ItemBase
    {
        public WeaponType WeaponType { get; }

        public WeaponAttributes Attributes { get; set; }

        public Weapon(string name, int levelReq, WeaponType weaponType, WeaponAttributes weaponAttributes) : base(name, levelReq, ItemSlot.Weapon)
        {
            WeaponType = weaponType;
            Attributes = weaponAttributes;
        }
    }
}
