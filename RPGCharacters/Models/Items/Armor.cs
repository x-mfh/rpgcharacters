﻿namespace RPGCharacters.Models.Items
{
    public class Armor : ItemBase
    {
        public ArmorType ArmorType { get; }

        public PrimaryAttributes PrimaryAttributes { get; }

        public Armor(string name, int levelReq, ItemSlot slot, ArmorType armorType, PrimaryAttributes primaryAttributes) : base(name, levelReq, slot)
        {
            ArmorType = armorType;
            PrimaryAttributes = primaryAttributes;
        }
    }
}
