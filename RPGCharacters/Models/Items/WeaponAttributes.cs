﻿namespace RPGCharacters.Models.Items
{
    public class WeaponAttributes
    {
        public double Damage { get; set; }
        public double AttackSpeed { get; set; }
        public double DPS { get { return Damage * AttackSpeed; } }
    }
}
