﻿namespace RPGCharacters.Models.Items
{
    public abstract class ItemBase
    {
        public string Name { get; }
        public int LevelRequirement { get; }
        public ItemSlot Slot { get; }

        public ItemBase(string name, int levelReq, ItemSlot slot)
        {
            Name = name;
            LevelRequirement = levelReq;
            Slot = slot;
        }
    }
}
