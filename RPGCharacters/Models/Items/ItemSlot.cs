﻿namespace RPGCharacters.Models.Items
{
    public enum ItemSlot
    {
        Head,
        Body,
        Legs,
        Weapon
    }
}
