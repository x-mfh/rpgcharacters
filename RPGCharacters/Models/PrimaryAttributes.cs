﻿using System;

namespace RPGCharacters.Models
{
    public class PrimaryAttributes
    {
        public double Strength { get; set; } = 0;
        public double Dexterity { get; set; } = 0;
        public double Intelligence { get; set; } = 0;
        public double Vitality { get; set; } = 0;

        public override bool Equals(object obj)
        {
            return obj is PrimaryAttributes attributes &&
                   Strength == attributes.Strength &&
                   Dexterity == attributes.Dexterity &&
                   Intelligence == attributes.Intelligence &&
                   Vitality == attributes.Vitality;
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(Strength, Dexterity, Intelligence, Vitality);
        }

        public static PrimaryAttributes operator +(PrimaryAttributes lhs, PrimaryAttributes rhs)
        {
            return new PrimaryAttributes
            {
                Strength = lhs.Strength + rhs.Strength,
                Dexterity = lhs.Dexterity + rhs.Dexterity,
                Intelligence = lhs.Intelligence + rhs.Intelligence,
                Vitality = lhs.Vitality + rhs.Vitality
            };
        }
    }
}
