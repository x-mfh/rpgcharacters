﻿using RPGCharacters.Models.Items;

namespace RPGCharacters.Models.Characters
{
    public class Mage : CharacterBase
    {
        public Mage(string name) : base(name)
        {
            BasePrimaryAttributes = new PrimaryAttributes { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };
            LevelUpPrimaryAttributes = new PrimaryAttributes { Vitality = 3, Strength = 1, Dexterity = 1, Intelligence = 5 };
            AddAvailableArmorTypes(ArmorType.Cloth);
            AddAvailableWeaponTypes(WeaponType.Staff, WeaponType.Wand);
        }

        protected override double CalculateTotalDPS(double weaponDPS)
        {
            return weaponDPS * (1 + (TotalPrimaryAttributes.Intelligence / 100));
        }
    }
}
