﻿using RPGCharacters.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Models.Characters
{
    public class Ranger : CharacterBase
    {
        public Ranger(string name) : base(name)
        {
            BasePrimaryAttributes = new PrimaryAttributes { Vitality = 8, Strength = 1, Dexterity = 7, Intelligence = 1 };
            LevelUpPrimaryAttributes = new PrimaryAttributes { Vitality = 2, Strength = 1, Dexterity = 5, Intelligence = 1 };
            AddAvailableArmorTypes(ArmorType.Mail, ArmorType.Leather);
            AddAvailableWeaponTypes(WeaponType.Bow);
        }

        protected override double CalculateTotalDPS(double weaponDPS)
        {
            return weaponDPS * (1 + (TotalPrimaryAttributes.Dexterity / 100));
        }
    }
}
