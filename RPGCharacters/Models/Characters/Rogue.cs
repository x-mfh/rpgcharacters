﻿using RPGCharacters.Models.Items;
using System;

namespace RPGCharacters.Models.Characters
{
    public class Rogue : CharacterBase
    {
        public Rogue(string name) : base(name)
        {
            BasePrimaryAttributes = new PrimaryAttributes { Vitality = 8, Strength = 2, Dexterity = 6, Intelligence = 1 };
            LevelUpPrimaryAttributes = new PrimaryAttributes { Vitality = 3, Strength = 1, Dexterity = 4, Intelligence = 1 };
            AddAvailableArmorTypes(ArmorType.Mail, ArmorType.Leather);
            AddAvailableWeaponTypes(WeaponType.Dagger, WeaponType.Sword);
        }

        protected override double CalculateTotalDPS(double weaponDPS)
        {
            return weaponDPS * (1 + (TotalPrimaryAttributes.Dexterity / 100));
        }
    }
}
