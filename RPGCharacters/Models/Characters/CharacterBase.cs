﻿using System;
using RPGCharacters.Models.Items;
using System.Collections.Generic;
using System.Linq;
using RPGCharacters.Models.Exceptions;
using System.Text;

namespace RPGCharacters.Models.Characters
{
    public abstract class CharacterBase
    {
        public string Name { get; }
        public int Level { get; protected set; } = 1;
        protected Dictionary<ItemSlot, ItemBase> Equipment { get; set; } = new Dictionary<ItemSlot, ItemBase>();
        protected List<WeaponType> AvailableWeaponTypes { get; set; } = new List<WeaponType>();
        protected List<ArmorType> AvailableArmorTypes { get; set; } = new List<ArmorType>();
        public PrimaryAttributes BasePrimaryAttributes { get; protected set; }
        protected PrimaryAttributes LevelUpPrimaryAttributes { get; set; }
        public PrimaryAttributes TotalPrimaryAttributes 
        { 
            get { return CalculateTotalPrimaryAttributes(); }
        }

        public SecondaryAttributes SecondaryAttributes
        {
            get { return CalculateSecondaryAttributes(); }
        }

        public CharacterBase(string name)
        {
            Name = name;
        }

        /// <summary>
        /// Increments character level by given levels.
        /// Calls AddBasePrimaryAttributesOnLevelUp() with levels given.
        /// </summary>
        /// <param name="levels">The amount of levels to increment by.</param>
        /// <exception cref="ArgumentException">When levels is less than 1.</exception>
        public void LevelUp(int levels = 1)
        {
            // Checks if levels is less than 1.
            if (levels < 1)
                throw new ArgumentException("Level must be above 0.");

            // Adds levels to current character level.
            Level += levels;
            AddBasePrimaryAttributesOnLevelUp(levels);
        }

        /// <summary>
        /// Called by LevelUp().
        /// Increments BasePrimaryAttributes based on LevelUpPrimaryAttributes given by different classes.
        /// </summary>
        /// <param name="level">The amount of times to increment BasePrimaryAttributes.</param>
        private void AddBasePrimaryAttributesOnLevelUp(int levels)
        {
            for (int i = 0; i < levels; i++)
            {
                BasePrimaryAttributes += LevelUpPrimaryAttributes;
            }
        }

        /// <summary>
        /// Add new ArmorTypes to AvailableArmorTypes, if not already in collection.
        /// </summary>
        /// <param name="armorTypes">ArmorTypes that is added to AvailableArmorTypes.</param>
        protected void AddAvailableArmorTypes(params ArmorType[] armorTypes)
        {
            // Loops through all the ArmorTypes.
            foreach (ArmorType armorType in armorTypes)
            {
                // Checks if ArmorType is already in AvailableArmorTypes.
                if (!AvailableArmorTypes.Contains(armorType))
                {
                    // If it's not, add the ArmorType to AvailableArmorTypes.
                    AvailableArmorTypes.Add(armorType);
                }
            }
        }

        /// <summary>
        /// Add new WeaponTypes to AvailableWeaponTypes, if not already in collection.
        /// </summary>
        /// <param name="weaponTypes">WeaponTypes that is added to AvailableWeaponTypes.</param>
        protected void AddAvailableWeaponTypes(params WeaponType[] weaponTypes)
        {
            // Loops through all the WeaponTypes.
            foreach (WeaponType weaponType in weaponTypes)
            {
                // Checks if WeaponType is already in AvailableWeaponTypes.
                if (!AvailableWeaponTypes.Contains(weaponType))
                {
                    // If it's not, add the WeaponType to AvailableWeaponTypes.
                    AvailableWeaponTypes.Add(weaponType);
                }
            }
        }

        /// <summary>
        /// Calculates the character's current DPS.
        /// Calls GetWeaponDPS() and CalculateTotalDPS().
        /// </summary>
        /// <returns>The character's current DPS.</returns>
        public double CalculateDPS()
        {
            // Gets weapon DPS from GetWeaponDPS().
            double weaponDPS = GetWeaponDPS();

            // Returns calculated dps based on character's equipped weapon dps and character's TotalPrimaryAttributes.
            return CalculateTotalDPS(weaponDPS);
        }

        /// <summary>
        /// Called by CalculateDPS().
        /// Gets equipped weapon dps.
        /// </summary>
        /// <returns>The equipped weapon dps, defaults to 1 if no weapon is equipped.</returns>
        private double GetWeaponDPS()
        {
            // Check if character has an weapon equipped.
            if (!Equipment.ContainsKey(ItemSlot.Weapon))
            {
                // If it hasn't, default weapon DPS to 1.
                return 1;
            }
            // If it has, get the current weapon equipped.
            Weapon equippedWeapon = (Weapon)Equipment.GetValueOrDefault(ItemSlot.Weapon);
            // Returns the equipped weapon DPS.
            return equippedWeapon.Attributes.DPS;
        }

        /// <summary>
        /// Called by CalculateDPS().
        /// Calculates total DPS in different classes based on class main PrimaryAttribute stat.
        /// </summary>
        /// <param name="weaponDPS">The weapon dps returned by GetWeaponDPS().</param>
        /// <returns>The character's current DPS.</returns>
        protected abstract double CalculateTotalDPS(double weaponDPS);

        /// <summary>
        /// Calls CanEquipItem() to check if character can equip weapon.
        /// If it can, it adds the weapon to the items equipment slot.
        /// </summary>
        /// <param name="weapon">The weapon to add to equipment.</param>
        /// <returns>Message based on result on CanEquipItem().</returns>
        public string EquipItem(Weapon weapon)
        {
            // Check if character can equip weapon.
            if (CanEquipItem(weapon))
            {
                // If it can, add the weapon to the weapon ItemSlot in Equipment.
                Equipment[weapon.Slot] = weapon;
                // Return a message that the weapon was successfully equipped.
                return "New weapon equipped!";
            }
            // Return a message that the weapon couldn't be equipped.
            // Currently can't reach this point, since CanEquipItem() throws exception with message,
            // if it fails.
            return "Can't equip this weapon!";
        }

        /// <summary>
        /// Calls CanEquipItem() to check if character can equip armor.
        /// If it can, it adds the armor to the items equipment slot.
        /// </summary>
        /// <param name="armor">The armor to add to equipment.</param>
        /// <returns>Message based on result on CanEquipItem().</returns>
        public string EquipItem(Armor armor)
        {
            // Check if character can equip armor.
            if (CanEquipItem(armor))
            {
                // If it can, add the armor to the armor ItemSlot in Equipment.
                Equipment[armor.Slot] = armor;
                // Return a message that the armor was successfully equipped.
                return "New armor equipped!";
            }
            // Return a message that the armor couldn't be equipped.
            // Currently can't reach this point, since CanEquipItem() throws exception with message,
            // if it fails.
            return "Can't equip this armor!";
        }

        /// <summary>
        /// Called by EquipItem().
        /// Checks if given weapon is useable by character.
        /// </summary>
        /// <param name="weapon">The weapon to check useablity for.</param>
        /// <returns>If character can use weapon.</returns>
        /// <exception cref="InvalidWeaponException">When weapon type is not in AvailableWeaponTypes 
        /// or weapon LevelRequirement is higher than character level.</exception>
        private bool CanEquipItem(Weapon weapon)
        {
            // Checks if the weapon's WeaponType is in characters AvailableWeaponTypes.
            if (!AvailableWeaponTypes.Contains(weapon.WeaponType))
            {
                throw new InvalidWeaponException("This class can't use that weapon type.");
            }
            // Checks if current character level is less than the weapon's LevelRequirement.
            if (Level < weapon.LevelRequirement)
            {
                throw new InvalidWeaponException("Character is too low level to equip this item.");
            }
            // If we get here, the character can equip the weapon.
            return true;
        }

        /// <summary>
        /// Called by EquipItem().
        /// Checks if given armor is useable by character.
        /// </summary>
        /// <param name="armor">The armor to check useablity for.</param>
        /// <returns>If character can use armor.</returns>
        /// <exception cref="InvalidArmorException">When armor type is not in AvailableArmorTypes 
        /// or armor LevelRequirement is higher than character level.</exception>
        private bool CanEquipItem(Armor armor)
        {
            // Checks if the armor's ArmorType is in characters AvailableArmorTypes.
            if (!AvailableArmorTypes.Contains(armor.ArmorType))
            {
                throw new InvalidArmorException("This class can't use that armor type.");
            }
            // Checks if current character level is less than the armor's LevelRequirement.
            if (Level < armor.LevelRequirement)
            {
                throw new InvalidArmorException("Character is too low level to equip this item.");
            }
            // If we get here, the character can equip the armor.
            return true;
        }

        /// <summary>
        /// Calculates character's TotalPrimaryAttributes.
        /// </summary>
        /// <returns>Character's total PrimaryAttributes from BasePrimaryAttributes and PrimaryAttributes from equipped armor.</returns>
        private PrimaryAttributes CalculateTotalPrimaryAttributes()
        {
            // Gets the character's equipped armor by filtering out the characters equipped weapon.
            List<Armor> armors = Equipment.Where(item => item.Value.Slot != ItemSlot.Weapon).Select(item => item.Value as Armor).ToList();
            // Initialize a variable for holding PrimaryAttributes to return.
            PrimaryAttributes calculatedPrimaryAttributes = new PrimaryAttributes();
            // Add character's BasePrimaryAttributes to return variable.
            calculatedPrimaryAttributes += BasePrimaryAttributes;

            // Loops through all the character's equipped armor.
            foreach (Armor armor in armors)
            {
                // Add armor's PrimaryAttributes to return variable.
                calculatedPrimaryAttributes += armor.PrimaryAttributes;
            }
            // Return the variable holding the characters total PrimaryAttributes.
            return calculatedPrimaryAttributes;
        }

        /// <summary>
        /// Calculates character's SecondaryAttributes.
        /// </summary>
        /// <returns>SecondaryAttributes based on character's TotalPrimaryAttributes.</returns>
        private SecondaryAttributes CalculateSecondaryAttributes()
        {
            // Saves TotalPrimaryAttributes to prevent CalculateTotalPrimaryAttributes() getting called
            // everytime TotalPrimaryAttributes gets accessed.
            PrimaryAttributes totalPrimaryAttributes = TotalPrimaryAttributes;
            // Initialize a variable with SecondaryAttributes and calculate it's attributes,
            // based on the character's TotalPrimaryAttributes.
            SecondaryAttributes calculatedSecondaryAttributes = new SecondaryAttributes()
            {
                Health = totalPrimaryAttributes.Vitality * 10,
                ArmorRating = totalPrimaryAttributes.Strength + totalPrimaryAttributes.Dexterity,
                ElementalResistance = totalPrimaryAttributes.Intelligence
            };
            // Return the variable holding the character's SecondaryAttributes.
            return calculatedSecondaryAttributes;
        }
    }
}
