﻿using RPGCharacters.Models.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGCharacters.Models.Characters
{
    public class Warrior : CharacterBase
    {
        public Warrior(string name) : base(name)
        {
            BasePrimaryAttributes = new PrimaryAttributes { Vitality = 10, Strength = 5, Dexterity = 2, Intelligence = 1 };
            LevelUpPrimaryAttributes = new PrimaryAttributes { Vitality = 5, Strength = 3, Dexterity = 2, Intelligence = 1 };
            AddAvailableArmorTypes(ArmorType.Mail, ArmorType.Plate);
            AddAvailableWeaponTypes(WeaponType.Axe, WeaponType.Hammer, WeaponType.Sword);
        }

        protected override double CalculateTotalDPS(double weaponDPS)
        {
            return weaponDPS * (1 + (TotalPrimaryAttributes.Strength / 100));
        }
    }
}
