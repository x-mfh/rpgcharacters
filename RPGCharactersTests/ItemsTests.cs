﻿using RPGCharacters.Models;
using RPGCharacters.Models.Characters;
using RPGCharacters.Models.Exceptions;
using RPGCharacters.Models.Items;
using Xunit;

namespace RPGCharactersTests
{
    public class ItemsTests
    {
        #region Equip items
        [Fact]
        public void EquipItem_WeaponLevelRequirementToHigh_ShouldThrowInvalidWeaponExceptionWithMessage()
        {
            // Arrange
            string expectedMessage = "Character is too low level to equip this item.";
            Warrior warrior = new Warrior("Dewald");
            Weapon testAxe = new Weapon("Common Axe", 2, WeaponType.Axe, new WeaponAttributes { AttackSpeed = 1, Damage = 1 });
            // Act and assert
            InvalidWeaponException exception = Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testAxe));
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void EquipItem_ArmorLevelRequirementToHigh_ShouldThrowInvalidArmorExceptionWithMessage()
        {
            // Arrange
            string expectedMessage = "Character is too low level to equip this item.";
            Warrior warrior = new Warrior("Dewald");
            Armor testPlateArmor = new Armor("Common plate body armor", 2, ItemSlot.Body, ArmorType.Plate, new PrimaryAttributes());
            // Act and assert
            InvalidArmorException exception = Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testPlateArmor));
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void EquipItem_WrongWeaponType_ShouldThrowInvalidWeaponExceptionWithMessage()
        {
            // Arrange
            string expectedMessage = "This class can't use that weapon type.";
            Warrior warrior = new Warrior("Dewald");
            Weapon testBow = new Weapon("Common Bow", 1, WeaponType.Bow, new WeaponAttributes { AttackSpeed = 1, Damage = 1 });
            // Act and assert
            InvalidWeaponException exception = Assert.Throws<InvalidWeaponException>(() => warrior.EquipItem(testBow));
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void EquipItem_WrongArmorType_ShouldThrowInvalidArmorExceptionWithMessage()
        {
            // Arrange
            string expectedMessage = "This class can't use that armor type.";
            Warrior warrior = new Warrior("Dewald");
            Armor testClothArmor = new Armor("Common cloth body armor", 2, ItemSlot.Body, ArmorType.Cloth, new PrimaryAttributes());
            // Act and assert
            InvalidArmorException exception = Assert.Throws<InvalidArmorException>(() => warrior.EquipItem(testClothArmor));
            Assert.Equal(expectedMessage, exception.Message);
        }

        [Fact]
        public void EquipItem_ValidWeapon_ShouldReturnSuccessMessage()
        {
            // Arrange
            string expected = "New weapon equipped!";
            Warrior warrior = new Warrior("Dewald");
            Weapon testAxe = new Weapon("Common Axe", 1, WeaponType.Axe, new WeaponAttributes { AttackSpeed = 1, Damage = 1 });
            // Act
            string actual = warrior.EquipItem(testAxe);
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void EquipItem_ValidArmor_ShouldReturnSuccessMessage()
        {
            // Arrange
            string expected = "New armor equipped!";
            Warrior warrior = new Warrior("Dewald");
            Armor testPlateArmor = new Armor("Common plate body armor", 1, ItemSlot.Body, ArmorType.Plate, new PrimaryAttributes());
            // Act
            string actual = warrior.EquipItem(testPlateArmor);
            // Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region Calculate DPS

        [Fact]
        public void CalculateDPS_NoWeaponEquipped_ShouldDefaultWeaponDPSToOne()
        {
            // Arrange
            double primaryAttribute = 5;
            double expected = 1 * (1 + (primaryAttribute / 100));
            Warrior warrior = new Warrior("Dewald");
            // Act
            double actual = warrior.CalculateDPS();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPS_ValidWeaponEquipped_ShouldCalculateWithWeaponDPS()
        {
            // Arrange
            double weaponAttackSpeed = 1.1;
            double weaponDamage = 7;
            double primaryAttribute = 5;
            double expected = (weaponAttackSpeed * weaponDamage) * (1 + (primaryAttribute / 100));
            Warrior warrior = new Warrior("Dewald");
            Weapon testAxe = new Weapon("Common Axe", 1, WeaponType.Axe, new WeaponAttributes { AttackSpeed = weaponAttackSpeed, Damage = weaponDamage });
            warrior.EquipItem(testAxe);
            // Act
            double actual = warrior.CalculateDPS();
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CalculateDPS_ValidWeaponAndArmorEquipped_ShouldCalculateWithWeaponDPSAndArmorPrimaryAttributes()
        {
            // Arrange
            double weaponAttackSpeed = 1.1;
            double weaponDamage = 7;
            double primaryAttribute = 6;
            double expected = (weaponAttackSpeed * weaponDamage) * (1 + (primaryAttribute / 100));
            Warrior warrior = new Warrior("Dewald");
            Weapon testAxe = new Weapon("Common Axe", 1, WeaponType.Axe, new WeaponAttributes { AttackSpeed = weaponAttackSpeed, Damage = weaponDamage });
            Armor testPlateArmor = new Armor("Common plate body armor", 1, ItemSlot.Body, ArmorType.Plate, new PrimaryAttributes { Strength = 1 });
            warrior.EquipItem(testAxe);
            warrior.EquipItem(testPlateArmor);
            // Act
            double actual = warrior.CalculateDPS();
            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion
    }
}
