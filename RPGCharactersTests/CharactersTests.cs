using RPGCharacters.Models;
using RPGCharacters.Models.Characters;
using System;
using Xunit;

namespace RPGCharactersTests
{
    public class CharactersTests
    {
        #region Character default level and level up
        [Fact]
        public void Constructor_DefaultLevel_ShouldReturnOne()
        {
            // Arrange
            int expected = 1;
            // Act
            CharacterBase character = new Mage("Dewald");
            int actual = character.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_NoParameter_ShouldIncrementByOne()
        {
            // Arrange
            int expected = 2;
            CharacterBase character = new Mage("Dewald");
            // Act
            character.LevelUp();
            int actual = character.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_LevelUpBy5_ShouldIncreaseLevelBy5()
        {
            // Arrange
            int expected = 6;
            CharacterBase character = new Mage("Dewald");
            // Act
            character.LevelUp(5);
            int actual = character.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Theory]
        [InlineData(0)]
        [InlineData(-1)]
        public void LevelUp_InvalidLevel_ShouldThrowArgumentExceptionWithMessage(int level)
        {
            // Arrange
            string expectedMessage = "Level must be above 0.";
            CharacterBase character = new Mage("Dewald");
            // Act and assert
            ArgumentException exception = Assert.Throws<ArgumentException>(() => character.LevelUp(level));
            Assert.Equal(expectedMessage, exception.Message);
        }

        #endregion

        #region Class Default Constructor
        [Fact]
        public void Constructor_Mage_ShouldInitializeBaseAttributes()
        {
            // Arrange
            PrimaryAttributes expected = new PrimaryAttributes { Vitality = 5, Strength = 1, Dexterity = 1, Intelligence = 8 };
            // Act
            Mage character = new Mage("Dewald");
            PrimaryAttributes actual = character.BasePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_Ranger_ShouldInitializeBaseAttributes()
        {
            // Arrange
            PrimaryAttributes expected = new PrimaryAttributes { Vitality = 8, Strength = 1, Dexterity = 7, Intelligence = 1 };
            // Act
            Ranger character = new Ranger("Dewald");
            PrimaryAttributes actual = character.BasePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_Rogue_ShouldInitializeBaseAttributes()
        {
            // Arrange
            PrimaryAttributes expected = new PrimaryAttributes { Vitality = 8, Strength = 2, Dexterity = 6, Intelligence = 1 };
            // Act
            Rogue character = new Rogue("Dewald");
            PrimaryAttributes actual = character.BasePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Constructor_Warrior_ShouldInitializeBaseAttributes()
        {
            // Arrange
            PrimaryAttributes expected = new PrimaryAttributes { Vitality = 10, Strength = 5, Dexterity = 2, Intelligence = 1 };
            // Act
            Warrior character = new Warrior("Dewald");
            PrimaryAttributes actual = character.BasePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }
        #endregion

        #region Class attributes increased on level up
        [Fact]
        public void LevelUp_Mage_ShouldIncreaseBasePrimaryAttributes()
        {
            // Arrange
            PrimaryAttributes expected = new PrimaryAttributes { Vitality = 8, Strength = 2, Dexterity = 2, Intelligence = 13 };
            Mage character = new Mage("Dewald");
            // Act
            character.LevelUp();
            PrimaryAttributes actual = character.BasePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Ranger_ShouldIncreaseBasePrimaryAttributes()
        {
            // Arrange
            PrimaryAttributes expected = new PrimaryAttributes { Vitality = 10, Strength = 2, Dexterity = 12, Intelligence = 2 };
            Ranger character = new Ranger("Dewald");
            // Act
            character.LevelUp();
            PrimaryAttributes actual = character.BasePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Rogue_ShouldIncreaseBasePrimaryAttributes()
        {
            // Arrange
            PrimaryAttributes expected = new PrimaryAttributes { Vitality = 11, Strength = 3, Dexterity = 10, Intelligence = 2 };
            Rogue character = new Rogue("Dewald");
            // Act
            character.LevelUp();
            PrimaryAttributes actual = character.BasePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void LevelUp_Warrior_ShouldIncreaseBasePrimaryAttributes()
        {
            // Arrange
            PrimaryAttributes expected = new PrimaryAttributes { Vitality = 15, Strength = 8, Dexterity = 4, Intelligence = 2 };
            Warrior character = new Warrior("Dewald");
            // Act
            character.LevelUp();
            PrimaryAttributes actual = character.BasePrimaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void SecondaryStats_OnLevelUp_ShouldCalculateSecondaryStats()
        {
            // Arrange
            SecondaryAttributes expected = new SecondaryAttributes { Health = 150, ArmorRating = 12, ElementalResistance = 2 };
            Warrior character = new Warrior("Dewald");
            // Act
            character.LevelUp();
            SecondaryAttributes actual = character.SecondaryAttributes;
            // Assert
            Assert.Equal(expected, actual);
        }

        #endregion
    }
}
